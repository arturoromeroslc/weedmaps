import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { injectGlobal } from 'styled-components';
import App from './components/App';
import configureStore from './store/configureStore';
/* eslint-disable no-unused-expressions */
injectGlobal`
  body {
    margin: 0;
    padding: 0;
    min-width: 320px;
    font-family: sans-serif;
  }

  .half:before {
    position: absolute;
    overflow: hidden;
    display: block;
    z-index: 1;
    top: -12px; left: 0;
    width: 50%;
    content: attr(data-forhalf);
    color: black;
  }
`;
/* eslint-enable no-unused-expressions */

const cacheStore = window.localStorage.getItem('store') || {};
const store = configureStore(cacheStore);

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
