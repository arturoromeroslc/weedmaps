import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledAvatar = styled.img`
  width: 5rem;
  height: 5rem;
  border-radius: 2.5rem;
  margin: 1rem;
`;

const Avatar = ({ src, alt }) => (
  <StyledAvatar alt={alt} src={src} />
 );

Avatar.propTypes = {
  alt: PropTypes.string,
  src: PropTypes.string
};

Avatar.defaultProps = {
  alt: '',
  src: ''
};

export default Avatar;
