import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledHeader = styled.div`
  height: 100px;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
> h2 > p
  margin: 0;
  padding: 0;
`;

const LocationHeader = ({ location }) => (
  <StyledHeader data-qa="location-header">
    <h2>{location.name}</h2>
    <p>{location.quote}</p>
  </StyledHeader>
 );

LocationHeader.propTypes = {
  location: PropTypes.object
};

LocationHeader.defaultProps = {
  location: {}
};

export default LocationHeader;
