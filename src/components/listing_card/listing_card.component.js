import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import styled from 'styled-components';
import Avatar from '../avatar/avatar.component';

const FULL_STAR_CHARATER = '★';
const EMPTY_STAR_CHARACTER = '☆';

const StyledCardContent = styled.div`
  display: flex;
  margin: 2px;
`;

const StyledInfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: left;
`;

const StyledInfoName = styled.h3`
  font-size: 1rem;
  width: 14rem;
  font-weight: 600;
  line-height: 1.4rem;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: normal;
  margin-bottom: 0;
`;

const StyledInfoRating = styled.p`
  font-size: .8rem;
  margin-top: -0.75rem;
  margin-bottom: 0;
`;

const Rating = ({ number }) => {
  const stars = [];
  const roundedNumber = Math.round(number * 100) / 100;

  for (let i = 0; i < roundedNumber; i += 1) {
    stars.push({ full: true });
  }

  if (roundedNumber % 1 !== 0) {
    stars[stars.length - 1].full = false;
  }

  return (
    <span>
      {stars.length > 0 ?
        stars.map(star => (
          <span
            style={{ position: 'relative' }}
            className={!star.full && 'half'}
            key={shortid.generate()}
            data-forhalf={FULL_STAR_CHARATER}
          >
            {star.full ? FULL_STAR_CHARATER : EMPTY_STAR_CHARACTER}
          </span>
        )
      ) : 'No Reviews'}
    </span>
  );
};

Rating.propTypes = {
  number: PropTypes.number
};

Rating.defaultProps = {
  number: 0
};

const ListingCard = ({ listing }) => (
  <StyledCardContent>
    <Avatar alt={`${listing.type} - ${listing.name}`} src={listing.avatar_image.small_url}/>
    <StyledInfoContainer>
      <StyledInfoName>{listing.name}</StyledInfoName>
      <StyledInfoRating>
        <Rating
          number={listing.rating}
        /> | <span>
          {Math.round(listing.distance * 100) / 100} miles
        </span>
      </StyledInfoRating>
    </StyledInfoContainer>
  </StyledCardContent>
);

ListingCard.propTypes = {
  listing: PropTypes.object
};

ListingCard.defaultProps = {
  listings: {}
};

export default ListingCard;
