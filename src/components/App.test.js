import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import { App } from './App';

const location = { name: 'Portland', state: 'OR', quote: 'Beards welcome!' };
const regions = { doctor: { name: 'Dr. Feels Good' } };

describe('<App/>', () => {
  it('only show logo and button on initial render', () => {
    const component = renderer.create(<App isLocating={false} location={null} regions={null} />);

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('show Locating when isLocating is true', () => {
    const wrapper = mount(<App isLocating location={null} regions={null} />);
    const main = wrapper.find('[data-qa="app-main"]');

    expect(main.children().length).toEqual(2);
    expect(main.containsAllMatchingElements([<p>Locating...</p>])).toEqual(true);
  });

  it('show LocationHeader when location is known', () => {
    const wrapper = mount(<App isLocating={false} location={location} regions={regions} />);
    const main = wrapper.find('[data-qa="app-main"]');
    const locationHeader = main.find('[data-qa="location-header"]');

    expect(locationHeader.containsAllMatchingElements([
      <h2>Portland</h2>,
      <p>Beards welcome!</p>
    ])).toEqual(true);
  });

  it('fetch user\'s location on locate me click', () => {
    global.navigator.geolocation = {
      getCurrentPosition: jest.fn(),
      watchPosition: jest.fn()
    };

    const wrapper = mount(<App isLocating={false} location={location} regions={regions} />);
    const button = wrapper.find('[data-qa="locate-me"]');
    button.simulate('click');
    expect(global.navigator.geolocation.getCurrentPosition.mock.calls.length).toBe(1);
  });
});
