import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import get from 'lodash.get';
import styled from 'styled-components';
import { locate } from '../actions';
import logo from '../assets/logo.png';
import Button from './button/button.component';
import ListingCards from './listing_cards/listing_cards.component';
import LocationHeader from './location_header/location_header.component';

const StyledApp = styled.div`
  margin-bottom: 20px;
  text-align: center;
`;

const StyledHeader = styled.header`
  height: 70px;
  display: flex;
  padding: 0 20px;
  justify-content: space-between;
  align-items: center;
  background-color: #222;
  color: #fff;
`;

const StyledImage = styled.img`
  width: 110px;
  height: 25px;
`;

const StyledAppMain = styled.main`
  width: 90%;
  margin: 0 auto;
`;

const StyledLoading = styled.p`
  margin: 20px 0;
  font-size: 16px;
  color: #333;
`;

const StyledRegions = styled.div`
  height: 40px;
  width: 100%;
  text-align: left;
  line-height: 40px;
`;

const regionTypes = ['delivery', 'dispensary', 'doctor'];
const regionLabels = {
  delivery: 'Deliveries',
  dispensary: 'Dispensaries',
  doctor: 'Doctors'
};

export class App extends Component {

  constructor() {
    super();
    this.state = {
      clientLocating: false
    };

    this.locateMeClick = this.locateMe.bind(this);
  }

  locateMe() {
    const { dispatch } = this.props;

    if (navigator.geolocation) {
      this.setState({ ...this.state, clientLocating: true });
      navigator.geolocation.getCurrentPosition((position) => {
        dispatch(locate(position.coords));
      });
    }
  }

  render() {
    const { isLocating, location, regions } = this.props;
    const { clientLocating } = this.state;

    const getLabel = (listings, label) => {
      if (get(listings, 'listings', []).length > 0) {
        return (
          <div key={label} className="app__regions__content-label">
            <strong> {label} </strong>
          </div>
        );
      }
      return <div/>;
    };

    return (
      <StyledApp>
        <StyledHeader>
          <StyledImage src={logo} alt="weedmaps logo" width="300" height="70" />
          <Button data-qa="locate-me" text={'Locate Me'} onClick={this.locateMeClick} />
        </StyledHeader>
        <StyledAppMain data-qa="app-main">
          { (clientLocating || isLocating) && !location &&
            <StyledLoading>Locating...</StyledLoading>
          }
          { !isLocating && location &&
            <LocationHeader location={location}/>
          }
          { !isLocating && regions &&
            <StyledRegions>
              { regionTypes.map(regionType => (
                <div key={regionType}>
                  {getLabel(regions[regionType], regionLabels[regionType])}
                  <ListingCards listings={get(regions[regionType], 'listings')}/>
                </div>
                  ))
              }
            </StyledRegions>
          }
        </StyledAppMain>
      </StyledApp>
    );
  }
}

const mapStateToProps = state => state.location;

App.propTypes = {
  isLocating: PropTypes.bool.isRequired,
  location: PropTypes.object,
  regions: PropTypes.object,
  dispatch: PropTypes.any
};

App.defaultProps = {
  isLocating: false,
  location: {},
  regions: {}
};

export default connect(mapStateToProps)(App);
