import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ListingCard from '../listing_card/listing_card.component';

const StyledDiv = styled.div`
  margin: 10px 0 0 0;
  border: 1px solid black;
  @media (min-width: 761px) {
    min-width: 21.9rem;
  }
`;

const StyledWallContainer = styled.div`
  @media (min-width: 761px) {
    display: flex;
    max-width: 100%;
    flex-wrap: wrap;
    justify-content: space-evenly;
  }
`;

const ListingCards = ({ listings }) => (
  <StyledWallContainer>
    {listings.map(listing => (
      <StyledDiv key={listing.id}>
        <ListingCard listing={listing} />
      </StyledDiv>
    ))}
  </StyledWallContainer>
);

ListingCards.propTypes = {
  listings: PropTypes.array
};

ListingCards.defaultProps = {
  listings: []
};

export default ListingCards;
