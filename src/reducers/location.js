import { REQUEST, RECEIVE } from '../constants/ActionTypes';

const locationListing = (state = { isLocating: false, location: null, regions: null }, action) => {
 // TODO using the above state shape, implement this reducer. Don't forget to test it!
  switch (action.type) {
    case REQUEST: return { ...state, ...action.data, };
    case RECEIVE: return { ...state, ...action.data, };
    default: return state;
  }
};

export default locationListing;
