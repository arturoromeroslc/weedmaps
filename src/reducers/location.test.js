import reducer from './index';
import { REQUEST, RECEIVE } from '../constants/ActionTypes';

describe('location reducer', () => {
  // TODO Write some tests for the TODOs reducer
  it('return the initial state', () => {
    expect(reducer(undefined, {}))
    .toEqual({ location: { isLocating: false, location: null, regions: null } });
  });

  it('handle request location', () => {
    expect(
      reducer({}, {
        type: REQUEST,
        data: { isLocating: true, location: 11, regions: -22 }
      })
    ).toEqual({ location: { isLocating: true, location: 11, regions: -22 } });
  });

  it('handle receive location', () => {
    expect(
      reducer({}, {
        type: RECEIVE,
        data: { isLocating: false, location: { city: 'Portland' }, regions: { doctor: 'Dr. Feel Good' } }
      })
    ).toEqual({
      location: {
        isLocating: false,
        location: {
          city: 'Portland',
        },
        regions: {
          doctor: 'Dr. Feel Good',
        },
      }
    });
  });
});
