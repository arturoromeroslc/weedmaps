import configureStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';
import thunk from 'redux-thunk';
import { requestLocation, receiveLocation, locate } from './index';
import * as types from '../constants/ActionTypes';

const mockStore = configureStore([thunk]);
const store = mockStore({});
const coords = { latitude: 140, longitude: -55 };

describe('actions', () => {
  it('create location action', () => {
    store.dispatch(requestLocation(coords));

    const expectedAction = {
      type: types.REQUEST,
      data: { isLocating: true, location: coords },
    };

    expect(store.getActions()).toEqual([expectedAction]);
    store.clearActions();
  });

  it('create receive action', () => {
    store.dispatch(receiveLocation(coords));

    const expectedAction = {
      type: types.RECEIVE,
      data: { isLocating: false, location: coords, regions: {} },
    };

    expect(store.getActions()).toEqual([expectedAction]);
    store.clearActions();
  });

  it('create locate action', () => {
    const location = { city: 'Portland', state: 'Oregon', quote: 'The Dream of the 90s is alive' };
    const regions = { doctor: { name: 'East Portland' } };
    fetchMock.getOnce(/api\/v2\/location/, { data: { location, regions } });

    const expectedActions = [
      { type: types.REQUEST, data: { isLocating: true, location: coords } },
      { type: types.RECEIVE, data: { isLocating: false, location, regions } }
    ];

    return store.dispatch(locate(coords)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
      store.clearActions();
    });
  });
});
