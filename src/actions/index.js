import * as types from '../constants/ActionTypes';
import { API_HOST } from '../constants/config';

/* TODO Given the state shape defined in the reducer,
 * implement this action
 */
export const requestLocation = ({ latitude, longitude }) => ({
  type: types.REQUEST,
  data: { isLocating: true, location: { latitude, longitude } },
});

/* TODO Given the state shape defined in the reducer,
 * implement this action
 */
export const receiveLocation = (location = {}, regions = {}) => ({
  type: types.RECEIVE,
  data: { isLocating: false, location, regions },
});

/* TODO Given the state shape defined in the reducer,
 * implement this action. Hint, this Action will likely be async
 */
export const locate = coords => (dispatch) => {
  dispatch(requestLocation(coords));
  const { latitude, longitude } = coords;
  return fetch(`https://${API_HOST}/api/v2/location?include[]=regions.listings&latlng=${latitude},${longitude}`)
    .then(res => res.json())
    .then(json => dispatch(receiveLocation(json.data.location, json.data.regions)))
    .catch((err) => {
      console.error(err);
      return dispatch(receiveLocation());
    });
};
