> The purpose of this test is to evaluate your front-end development skills. Expect to spend approximately 2-3 hours to complete, but there is no penalty for taking longer. Please do not include your .git history or node_modules directory when returning.

**Table of Contents**
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Available Scripts](#available-scripts)
- [Requirements](#requirements)
  - [Wireframe](#wireframe)
- [Architecture](#architecture)
- [Bonus](#bonus)
- [Weedmaps Location API Documentation](#weedmaps-location-api-documentation)
  - [Parameters](#parameters)
  - [Example Request](#example-request)
  - [Example Response](#example-response)

## Prerequisites
- **You'll need Node >= 7 installed.** We recommend leveraging [nvm](https://github.com/creationix/nvm) to switch Node versions between projects.
- Install [yarn](https://yarnpkg.com/en/docs/install) if you don't already have it
- If you experience issues running tests and you have [watchman](https://facebook.github.io/watchman) installed, you'll want to try installing version `4.7.0` or newer, or troubleshooting [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#troubleshooting).

## Installation
- Ensure you're running a modern version of NodeJS as mentioned above
- From this project root, run `$ yarn install`
- To start the development server, run `$ yarn start`
- Your browser should automatically open at `http://localhost:3000` and will reload on subsequent file changes

## Available Scripts
| Command | Description |
| - | - |
| `yarn start` | Starts the development server |
| `yarn test` | Starts the test runner |

## Requirements
- When a User navigates to the app for the first time, they should be presented with a simple user-interface that calls the [Weedmaps Location API](#weedmaps-location-api-documentation) when clicking on the `Locate Me` button.
- While the Location data is being fetched, an indication of progress should be displayed to the User. Upon successful response, the Location `name` and `quote` should be displayed on the page and below that a List of any Dispensaries, Deliveries, or Doctors returned for the region.
- The results displayed in the List component should be grouped by listing type and each listing should display the following attributes: `name`, `avatar_image`, `rating`, and `distance`.
- Implement state and component testing with [Jest](https://facebook.github.io/jest/)

### Wireframe
![wireframe](wireframe.png)

## Architecture
The state of the application should be managed using [Redux](http://redux.js.org). Components should utilize data from a Redux store and the general structure of the application has been provided and should look similar to the following:

```
src
├── actions
│   ├── actions.test.js
│   └── index.js
├── assets
│   └── logo.png
├── components
│   ├── App.css
│   ├── App.js
│   ├── App.test.js
│   ├── avatar
│   │   └── avatar.component.js
│   ├── button
│   │   ├── button.component.js
│   │   └── button.css
│   ├── listing_card
│   │   ├── listing_card.component.js
│   │   └── listing_card.css
│   ├── listing_cards
│   │   └── listing_cards.component.js
│   └── location_header
│       └── location_header.component.js
├── constants
│   ├── ActionTypes.js
│   └── config.js
├── index.js
├── reducers
│   ├── index.js
│   ├── location.js
│   └── location.test.js
└── store
    └── configureStore.js
```

## Bonus
- Render Listing rating value as Stars instead of numerical value without using an external library
- Convert vanilla CSS to `styled-components`
- Leverage browser Geolocation API when sending `latlng` params to Location API

## Weedmaps Location API Documentation
Given a request, this API will return the best matching location along with regions and/or listings that are published for each Listing type `(dispensary|delivery|doctor)`. If no parameters are provided the API will rely on the remote IP address to do a lookup for latitude and longitude.

### Parameters
|Name|Description|
|-|-|
|`include[]`|`regions` returns an array of regions for the given location. `regions.listings` includes the top 20 listings for a given region. Example: `include[]=regions.listings`|
|`latlng`|latitude,longitude formatted parameter. Example: `latlng=33.666614,-117.756295`|

#### Example Request
```shell
$ curl -X GET "https://api-v2.weedmaps.com/api/v2/location?include%5B%5D=regions.listings&latlng=33.666614%2C-117.756295" -H "accept: application/json"
```

#### Example Response
```json
{
  "data": {
    "location": {
      "name": "Irvine, CA",
      "city": "Irvine",
      "state": "California",
      "state_abv": "CA",
      "zipcode": "92618",
      "latitude": 33.666614,
      "longitude": -117.756295,
      "place_path": "/earth/us/ca/irvine/92618",
      "is_medical": true,
      "is_recreational": false,
      "quote": "Medically Legal Since 1996",
      "cta_message": null
    },
    "regions": {
      "dispensary": {
        "id": 166,
        "name": "Irvine",
        "slug": "lake-forest",
        "region_path": "united-states/california/lake-forest",
        "latitude": 33.6833610534668,
        "longitude": -117.76638793945312,
        "top_left": {
          "latitude": 33.599185,
          "longitude": -117.8695
        },
        "bottom_right": {
          "latitude": 33.778546,
          "longitude": -117.670184
        },
        "listings": []
      },
      "delivery": {
        "id": 166,
        "name": "Irvine",
        "slug": "lake-forest",
        "region_path": "united-states/california/lake-forest",
        "latitude": 33.6833610534668,
        "longitude": -117.76638793945312,
        "top_left": {
          "latitude": 33.599185,
          "longitude": -117.8695
        },
        "bottom_right": {
          "latitude": 33.778546,
          "longitude": -117.670184
        },
        "listings": [{
            "id": 600,
            "wmid": 493289257,
            "name": "Rite Greens Delivery - Irvine",
            "slug": "rite-greens-santa-ana",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/600/square_black_avatar.jpg"
            },
            "rating": 4.68117154811716,
            "distance": 0.6111718755293625,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.670623/-117.765768/402/147/map.png",
            "feature_order": 1,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 308,
            "wmid": 677650197,
            "name": "OCPC",
            "slug": "ocpc",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/308/square_OCPC_logo.jpg"
            },
            "rating": 4.97692307692308,
            "distance": 4.087415251487135,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.712116/-117.80172/402/147/map.png",
            "feature_order": 2,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 1568,
            "wmid": 729695274,
            "name": "1 Stop South OC",
            "slug": "onestopoc-2-4",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/1568/square_square_One_Stop_Logo_1200_DPI_-_Copy.jpg"
            },
            "rating": 5,
            "distance": 2.114078744901214,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.642706/-117.733361/402/147/map.png",
            "feature_order": 3,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 2603,
            "wmid": 323691855,
            "name": "1-866-DELIVERY",
            "slug": "deliverygreens-com-uci-campus",
            "type": "delivery",
            "city": "Irvine",
            "state": "California",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/2603/square_DELIVERY.jpg"
            },
            "rating": 4.98,
            "distance": 4.874999385662402,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.642239/-117.835831/402/147/map.png",
            "feature_order": 4,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 241,
            "wmid": 977643021,
            "name": "SCQM Delivery Irvine UCI",
            "slug": "scqmirvine",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/241/square_scqm-logo-3.jpg"
            },
            "rating": 4.86190476190476,
            "distance": 4.17041691398805,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.6588951/-117.8282121/402/147/map.png",
            "feature_order": 5,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 8153,
            "wmid": 465540215,
            "name": "KUSHAGRAM",
            "slug": "kushagram",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/8153/square_image.png"
            },
            "rating": 4.91567628749293,
            "distance": 3.810599705425062,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.682873/-117.819615/402/147/map.png",
            "feature_order": 6,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 7179,
            "wmid": 414605619,
            "name": "Green Avenue Group - Irvine",
            "slug": "green-avenue-group-irvine",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/7179/square_Green-Avenue_LOGO.jpg"
            },
            "rating": 4.96521739130435,
            "distance": 3.0734814484376782,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.705575/-117.730508/402/147/map.png",
            "feature_order": 7,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 3273,
            "wmid": 360055517,
            "name": "Organix Delivery - Irvine",
            "slug": "organix-delivery-irvine",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/3273/square_ORGANIXround.png"
            },
            "rating": 4.68279569892473,
            "distance": 4.53936821117429,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.73223/-117.760113/402/147/map.png",
            "feature_order": 8,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 16308,
            "wmid": 683651891,
            "name": "THE FYRE HAUS - IRVINE",
            "slug": "the-fyre-haus-irvine",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/16308/square_FyreHausLogoGreen_RED.png"
            },
            "rating": 4.99333333333333,
            "distance": 1.85676155870394,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.669712/-117.788366/402/147/map.png",
            "feature_order": 9,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 12701,
            "wmid": 164678418,
            "name": "The Herb Collective - Irvine",
            "slug": "the-herb-collective-2",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/12701/square_Herbal22.png"
            },
            "rating": 4.98333333333333,
            "distance": 1.560674442484825,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.647426/-117.741982/402/147/map.png",
            "feature_order": 10,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 6706,
            "wmid": 148412493,
            "name": "FAST N FRIENDLY (OPEN LATE!) - IRVINE",
            "slug": "fast-n-friendly-open-late-irvine",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/6706/square_FNF_LOGO.png"
            },
            "rating": 4.5125,
            "distance": 2.778087959559111,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.675107/-117.803513/402/147/map.png",
            "feature_order": 11,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 13509,
            "wmid": 268569023,
            "name": "949 Insta-Grams",
            "slug": "949insta-grams",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/13509/square_IMG_0564.JPG"
            },
            "rating": 4.96842105263158,
            "distance": 1.450527491178124,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.646149/-117.750681/402/147/map.png",
            "feature_order": 12,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 4647,
            "wmid": 555380772,
            "name": "OC Compassionate Care - Irvine",
            "slug": "oc-compassionate-care-irvine-2",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/4647/square_New_Logo_2016.jpg"
            },
            "rating": 4.70909090909091,
            "distance": 4.603181307931977,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.688463/-117.831918/402/147/map.png",
            "feature_order": 13,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 16274,
            "wmid": 899332429,
            "name": "GasCar - Irvine",
            "slug": "gas-car-2",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/16274/square_large_gascar_black.jpg"
            },
            "rating": 4.73,
            "distance": 0.6884145644594574,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.661754/-117.766744/402/147/map.png",
            "feature_order": 14,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 3864,
            "wmid": 338253056,
            "name": "QUIKBUDS Premium Delivery - Irvine",
            "slug": "quikbuds-premium-delivery-irvine",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/3864/square_Smaller_Quikbuds.jpg"
            },
            "rating": 5,
            "distance": 2.0718358485862565,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.68201/-117.787211/402/147/map.png",
            "feature_order": 15,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 17451,
            "wmid": 754595420,
            "name": "Black Diamond Delivery - Irvine",
            "slug": "black-diamond-delivery-irvine",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/17451/square_Logo-BD_Diamond_2.jpg"
            },
            "rating": 4.9875,
            "distance": 2.464326016072752,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.698875/-117.738026/402/147/map.png",
            "feature_order": 16,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 11579,
            "wmid": 675531607,
            "name": "Because We Care - Irvine",
            "slug": "because-we-care-irvine",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/11579/square_large_Because_We_Care.jpg"
            },
            "rating": 4.94444444444444,
            "distance": 3.3197769785989455,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.714497/-117.761006/402/147/map.png",
            "feature_order": 18,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 3410,
            "wmid": 231543055,
            "name": "PharmSquad- Irvine",
            "slug": "pharmsquad-irvine-2",
            "type": "delivery",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/3410/square_square_unnamed-5.png"
            },
            "rating": 4.92703862660944,
            "distance": 4.782486626009338,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.657689/-117.838755/402/147/map.png",
            "feature_order": 19,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 16619,
            "wmid": 286897534,
            "name": "Lickety Spliff - Irvine",
            "slug": "lickety-spliff",
            "type": "delivery",
            "city": "Irvine",
            "state": "Ca",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/16619/square_Screen_Shot_2016-11-04_at_7.24.02_PM.png"
            },
            "rating": 5,
            "distance": 4.463024546557465,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.648676/-117.830838/402/147/map.png",
            "feature_order": 9999,
            "license_type": "medical",
            "package_level": "basic"
          },
          {
            "id": 9580,
            "wmid": 524307836,
            "name": "Tops Cannabis - Irvine",
            "slug": "tops-cannabis-irvine-2",
            "type": "delivery",
            "city": "Irvine",
            "state": "Ca",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/deliveries/9580/square_large_TC_square_70.jpg"
            },
            "rating": 4.98044009779951,
            "distance": 1.0643616417085204,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.679359/-117.766689/402/147/map.png",
            "feature_order": 9999,
            "license_type": "medical",
            "package_level": "basic"
          }
        ]
      },
      "doctor": {
        "id": 121,
        "name": "Lake Forest",
        "slug": "south-oc",
        "region_path": "united-states/california/south-oc",
        "latitude": 33.655269622802734,
        "longitude": -117.68476104736328,
        "top_left": {
          "latitude": 33.593437,
          "longitude": -117.76145
        },
        "bottom_right": {
          "latitude": 33.725461,
          "longitude": -117.635637
        },
        "listings": [{
            "id": 12747,
            "wmid": 663091709,
            "name": "MMJRecs (100% Online!)",
            "slug": "venice-divan-inc-7",
            "type": "doctor",
            "city": "Online",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/doctors/12747/square_mmj_logo_full_dkblue.png"
            },
            "rating": 4.89565217391304,
            "distance": 2.545135879314055,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.681391/-117.796836/402/147/map.png",
            "feature_order": 1,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 13003,
            "wmid": 863775442,
            "name": "420Recs.com- Dana Point (100% Online)",
            "slug": "420recs-com-dana-point",
            "type": "doctor",
            "city": "Dana Point",
            "state": "ca",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/doctors/13003/square_420recs_new.png"
            },
            "rating": 3.66666666666667,
            "distance": 13.939363957836916,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.4699974/-117.702148/402/147/map.png",
            "feature_order": 3,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 12790,
            "wmid": 650445809,
            "name": "MMJ Doctor Online",
            "slug": "mmj-doctor-online-13",
            "type": "doctor",
            "city": "Online",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/doctors/12790/square_logo-for-social.png"
            },
            "rating": 5,
            "distance": 8.82674067935276,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.6636696/-117.9097326/402/147/map.png",
            "feature_order": 4,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 13831,
            "wmid": 168060930,
            "name": "RiseUpMD.com (100% Online)",
            "slug": "riseupmd-com-5",
            "type": "doctor",
            "city": "Dana Point",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/doctors/13831/square_riseupmd_logo.jpg"
            },
            "rating": 4.9968253968254,
            "distance": 4.254609576849923,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.6469749/-117.6861872/402/147/map.png",
            "feature_order": 5,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 13004,
            "wmid": 922970598,
            "name": "420Recs.com- Irvine (100% Online)",
            "slug": "420recs-com-irvine",
            "type": "doctor",
            "city": "Irvine",
            "state": "ca",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/doctors/13004/square_420recs_new.png"
            },
            "rating": 5,
            "distance": 5.718574840077831,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.707156/-117.843002/402/147/map.png",
            "feature_order": 6,
            "license_type": "medical",
            "package_level": "listing_plus"
          },
          {
            "id": 14100,
            "wmid": 734284480,
            "name": "Hybrid MD Urgent Care",
            "slug": "hybrid-md-urgent-care",
            "type": "doctor",
            "city": "San Clemente",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/doctors/14100/square_Weedmaps_Logo_1_.jpg"
            },
            "rating": 4.85,
            "distance": 17.03113948060651,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.4575805/-117.5995842/402/147/map.png",
            "feature_order": 9999,
            "license_type": "medical",
            "package_level": "basic"
          },
          {
            "id": 11388,
            "wmid": 633579079,
            "name": "MMOC - Medical Marijuana of Orange County",
            "slug": "mmoc-medical-marijuana-of-orange-county",
            "type": "doctor",
            "city": "Dana Point",
            "state": "CA",
            "avatar_image": {
              "small_url": "/assets/attachments_missing/avatars/doctors/square_missing.png"
            },
            "rating": 4.85,
            "distance": 12.592979035338649,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.4862457/-117.7250086/402/147/map.png",
            "feature_order": 9999,
            "license_type": "medical",
            "package_level": "basic"
          },
          {
            "id": 13988,
            "wmid": 552669943,
            "name": "Holistic On Call",
            "slug": "holistic-on-call-2",
            "type": "doctor",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "https://d2kxqxnk1i5o9a.cloudfront.net/uploads/avatars/doctors/13988/square_17634334_1669409650020544_3089614992377340104_n.jpg"
            },
            "rating": 5,
            "distance": 5.865149854361851,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.7514861/-117.7549295/402/147/map.png",
            "feature_order": 9999,
            "license_type": "medical",
            "package_level": "basic"
          },
          {
            "id": 12070,
            "wmid": 167219187,
            "name": "Medical Cannabis of Southern California",
            "slug": "medical-cannabis-of-socal",
            "type": "doctor",
            "city": "Irvine",
            "state": "CA",
            "avatar_image": {
              "small_url": "/assets/attachments_missing/avatars/doctors/square_missing.png"
            },
            "rating": 3.53333333333333,
            "distance": 6.149324303298355,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.6910251/-117.8591337/402/147/map.png",
            "feature_order": 9999,
            "license_type": "medical",
            "package_level": "free"
          },
          {
            "id": 13598,
            "wmid": 327825982,
            "name": "420EvaluationsOnline-Irvine",
            "slug": "420evaluationsonline-irvine",
            "type": "doctor",
            "city": "Irvine",
            "state": "ca",
            "avatar_image": {
              "small_url": "/assets/attachments_missing/avatars/doctors/square_missing.png"
            },
            "rating": 2.4,
            "distance": 5.485343109360303,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.6776868/-117.8507488/402/147/map.png",
            "feature_order": 9999,
            "license_type": "medical",
            "package_level": "free"
          },
          {
            "id": 10972,
            "wmid": 116795673,
            "name": "Dr. Ross",
            "slug": "ocmmes",
            "type": "doctor",
            "city": "Mission Viejo",
            "state": "CA",
            "avatar_image": {
              "small_url": "/assets/attachments_missing/avatars/doctors/square_missing.png"
            },
            "rating": 3,
            "distance": 6.744028879923663,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.599489/-117.671201/402/147/map.png",
            "feature_order": 9999,
            "license_type": "medical",
            "package_level": "free"
          },
          {
            "id": 11396,
            "wmid": 983084556,
            "name": "The Claudia Jenson MD Center for Integrated Medicine",
            "slug": "the-claudia-jenson-md-center-for-integrated-medicine-2",
            "type": "doctor",
            "city": "San Clemente",
            "state": "CA",
            "avatar_image": {
              "small_url": "/assets/attachments_missing/avatars/doctors/square_missing.png"
            },
            "rating": 0,
            "distance": 17.143891387345207,
            "static_map_url": "https://staticmap.weedmaps.com/static_map/13/33.453947/-117.602956/402/147/map.png",
            "feature_order": 9999,
            "license_type": "medical",
            "package_level": "free"
          }
        ]
      }
    }
  }
}
```
